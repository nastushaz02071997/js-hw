const emplyeeArr = [
    {
        id: 1,
        name: 'Денис',
        surname: 'Хрущ',
        salary: 1010, 
        workExperience: 10, 
        isPrivileges: false, 
        gender: 'male'
    },
    {
        id: 2,
        name: 'Сергей',
        surname: 'Войлов',
        salary: 1200, 
        workExperience: 12, 
        isPrivileges: false, 
        gender: 'male'
    },
    {
        id: 3,
        name: 'Татьяна',
        surname: 'Коваленко',
        salary: 480, 
        workExperience: 3, 
        isPrivileges: true, 
        gender: 'female'
    },
    {
        id: 4,
        name: 'Анна',
        surname: 'Кугир',
        salary: 2430, 
        workExperience: 20, 
        isPrivileges: false, 
        gender: 'female'
    },
    {
        id: 5,
        name: 'Татьяна',
        surname: 'Капустник',
        salary: 3150, 
        workExperience: 30, 
        isPrivileges: true, 
        gender: 'female'
    },
    {
        id: 6,
        name: 'Станислав',
        surname: 'Щелоков',
        salary: 1730, 
        workExperience: 15, 
        isPrivileges: false, 
        gender: 'male'
    },
    {
        id: 7,
        name: 'Денис',
        surname: 'Марченко',
        salary: 5730, 
        workExperience: 45, 
        isPrivileges: true, 
        gender: 'male'
    },
    {
        id: 8,
        name: 'Максим',
        surname: 'Меженский',
        salary: 4190, 
        workExperience: 39, 
        isPrivileges: false, 
        gender: 'male'
    },
    {
        id: 9,
        name: 'Антон',
        surname: 'Завадский',
        salary: 790, 
        workExperience: 7, 
        isPrivileges: false, 
        gender: 'male'
    },
    {
        id: 10,
        name: 'Инна',
        surname: 'Скакунова',
        salary: 5260, 
        workExperience: 49, 
        isPrivileges: true, 
        gender: 'female'
    },
    {
        id: 11,
        name: 'Игорь',
        surname: 'Куштым',
        salary: 300, 
        workExperience: 1, 
        isPrivileges: false, 
        gender: 'male'
    },
];
function Emploee (id,name, surname , salary, workExperience, isPrivileges, gender) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.salary = salary;
    this. workExperience = workExperience;
    this.isPrivileges = isPrivileges;
    this.gender = gender;
}
//ex1
const employeeObj = new Emploee(1,'Денис', 'Хрущ', 1010, 10, false, 'male');
console.log(employeeObj);
//ex2
employeeObj.getFullName = function() {
    return this.name + " " + this.surname;
    }
    console.log(employeeObj.getFullName());
    //ex3
    class Emplyee {
        constructor(emplyeeArr) {
            this.emplyeeArr = emplyeeArr
    
        }
        createEmployesFromArr() {
            return this.emplyeeArr
        }
    }
    let emplyee2 = new Emplyee(emplyeeArr);
    const emplyeeConstructArr = emplyee2.createEmployesFromArr();
    console.log(emplyeeConstructArr);

//ex4
    const getFullNamesFromArr = (arr) => {
        let len = arr.length
        let arrFullName = []
        for (let i = 0; i < len; i++) {
             arrFullName[i] = arr[i].name + ' ' + arr[i].surname
        }
        return arrFullName
    }
    console.log(getFullNamesFromArr(emplyeeConstructArr));
    
//ex5
const getMiddleSalary = (arr) => {
    let allSalary = 0
    let len = arr.length
    for (let i = 0; i < len; i++) {
        allSalary += arr[i].salary
    }
    let middleSalary = Math.floor(allSalary / len)
    return middleSalary
}
console.log(getMiddleSalary(emplyeeConstructArr))
